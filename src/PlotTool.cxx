#include "PlotTool.h"

//////////////
/// Public ///
//////////////
//==============================
PlotTool::PlotTool():
m_h(nullptr),           /// main histogram
m_j(nullptr),           /// result data (json)
m_h1(nullptr),          /// additional histogram
m_h2(nullptr),          /// additional histogram
m_dist(nullptr),          /// additional histogram
m_map(nullptr),          /// additional histogram
m_f(nullptr),           /// fitting function
m_c( nullptr ),         /// main canvas
m_fe_name(""),          /// AFE name
m_type(""),             /// plot type
m_histo_name(""),       /// histogram name
m_histo_type(""),       /// histogram type
m_data_name("data"),    /// result data name
m_chip_type("Asic"),    /// FE chip type
m_chip_name("JohnDoe"), /// FE chip name
m_title(""),
m_data_format(-1),      /// input data format
m_thr(0),               /// threshold //TODO
m_zeros(0),             /// zero counts //TODO
m_occs(0),              /// occupancy //TODO
m_nois(0),              /// noisy //TODO
m_percent( 95.0 ),
m_gaus( false )
{
#ifdef DEBUG
    std::cout << "PlotTool::PlotTool()" << std::endl;
#endif
};
PlotTool::~PlotTool() {
#ifdef DEBUG
    std::cout << "PlotTool::~PlotTool()" << std::endl;
#endif
    delete m_h;
    delete m_h1;
    delete m_h2;
    delete m_f;
};
//==============================
void PlotTool::build() {
#ifdef DEBUG
    std::cout << "PlotTool::build()" << std::endl;
#endif
    this->buildHisto();
    if      (m_data_format==0) this->fillHisto(m_j);
    else if (m_data_format==1) this->fillHisto((TH1*)m_h1);
    else if (m_data_format==2) this->fillHisto((TH2*)m_h2);
}
void PlotTool::write() {
#ifdef DEBUG
    std::cout << "PlotTool::write()" << std::endl;
#endif
    this->writeHisto();
}
void PlotTool::write(TH1* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::write(TH1)" << std::endl;
#endif
    m_h = (TH1*)i_h->Clone();
    m_h->SetName("Dist");
    this->write();
}
void PlotTool::write(TH2* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::write(TH2)" << std::endl;
#endif
    m_h = (TH2*)i_h->Clone();
    m_h->SetName("Map");
    this->write();
}
void PlotTool::print(std::string i_dir, bool i_print, std::string i_ext) {
#ifdef DEBUG
    std::cout << "PlotTool::print(" << i_dir << ", " << i_print << ", " << i_ext << ")" << std::endl;
#endif
    this->setCanvas();
    this->drawHisto();
    this->printHisto(i_dir, i_print, i_ext);
}
//==============================
void PlotTool::setParameters(json& i_data) {
#ifdef DEBUG
    std::cout << "PlotTool::setParameters(data)" << std::endl;
#endif
    if (!i_data["Type"].is_null()) m_histo_type = i_data["Type"];

    if (!i_data["x"]["Bins"].is_null()) m_axis.histo.x.nbins = i_data["x"]["Bins"];
    if (!i_data["x"]["Low"].is_null())  m_axis.histo.x.low   = i_data["x"]["Low"];
    if (!i_data["x"]["High"].is_null()) m_axis.histo.x.high  = i_data["x"]["High"];

    if (!i_data["y"]["Bins"].is_null()) m_axis.histo.y.nbins = i_data["y"]["Bins"];
    if (!i_data["y"]["Low"].is_null())  m_axis.histo.y.low   = i_data["y"]["Low"];
    if (!i_data["y"]["High"].is_null()) m_axis.histo.y.high  = i_data["y"]["High"];

    if (!i_data["z"]["Bins"].is_null()) m_axis.histo.z.nbins = i_data["z"]["Bins"];
    if (!i_data["z"]["Low"].is_null())  m_axis.histo.z.low   = i_data["z"]["Low"];
    if (!i_data["z"]["High"].is_null()) m_axis.histo.z.high  = i_data["z"]["High"];

    if (!i_data["x"]["AxisTitle"].is_null()) m_axis.histo.x.title = i_data["x"]["AxisTitle"];
    if (!i_data["y"]["AxisTitle"].is_null()) m_axis.histo.y.title = i_data["y"]["AxisTitle"];
    if (!i_data["z"]["AxisTitle"].is_null()) m_axis.histo.z.title = i_data["z"]["AxisTitle"];

    for (auto i : i_data["x"]["AxisLabel"]) {
        m_axis.histo.x.label.push_back(i);
    }
    for (auto i : i_data["y"]["AxisLabel"]) {
        m_axis.histo.y.label.push_back(i);
    }

    m_axis.overwrite = false;
    if (!i_data["Overwrite"].is_null()) m_axis.overwrite = i_data["Overwrite"];
};
void PlotTool::setParameters(TH1* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::setParameters(TH1)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.nbins = i_h->GetNbinsX();
    m_axis.histo.x.low   = i_h->GetXaxis()->GetXmin();
    m_axis.histo.x.high  = i_h->GetXaxis()->GetXmax();

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = i_h->GetBinContent(i_h->GetMinimumBin());
    m_axis.histo.y.high  = i_h->GetBinContent(i_h->GetMaximumBin());
    if (m_axis.histo.y.low==m_axis.histo.y.high) m_axis.histo.y.high+=1;

    m_axis.histo.x.title = i_h->GetXaxis()->GetTitle();
    m_axis.histo.y.title = i_h->GetYaxis()->GetTitle();
    m_axis.histo.z.title = i_h->GetZaxis()->GetTitle();

    if (i_h->GetXaxis()->GetLabels()) {
        for (int i=0; i<m_axis.histo.x.nbins; i++) {
            m_axis.histo.x.label.push_back(i_h->GetXaxis()->GetBinLabel(i+1));
        }
    }

    m_axis.overwrite = false;
};
void PlotTool::setParameters(TH2* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo2d";

    m_axis.histo.x.nbins = i_h->GetNbinsX();
    m_axis.histo.x.low   = i_h->GetXaxis()->GetXmin();
    m_axis.histo.x.high  = i_h->GetXaxis()->GetXmax();

    m_axis.histo.y.nbins = i_h->GetNbinsY();
    m_axis.histo.y.low   = i_h->GetYaxis()->GetXmin();
    m_axis.histo.y.high  = i_h->GetYaxis()->GetXmax();

    m_axis.histo.z.nbins = 1;
    m_axis.histo.z.low   = 0;
    m_axis.histo.z.high  = i_h->GetBinContent(i_h->GetMaximumBin());
    if (m_axis.histo.z.high==0) m_axis.histo.z.high = 1;

    m_axis.histo.x.title = i_h->GetXaxis()->GetTitle();
    m_axis.histo.y.title = i_h->GetYaxis()->GetTitle();
    m_axis.histo.z.title = i_h->GetZaxis()->GetTitle();

    if (i_h->GetXaxis()->GetLabels()) {
        for (int i=0; i<m_axis.histo.x.nbins; i++) {
            m_axis.histo.x.label.push_back(i_h->GetXaxis()->GetBinLabel(i+1));
        }
    }
    if (i_h->GetYaxis()->GetLabels()) {
        for (int i=0; i<m_axis.histo.y.nbins; i++) {
            m_axis.histo.y.label.push_back(i_h->GetYaxis()->GetBinLabel(i+1));
        }
    }

    m_axis.overwrite = false;
};
void PlotTool::setProjectionX(TH2* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::setProjectionX(TH2)" << std::endl;
#endif
    m_axis.histo.x.title = i_h->GetZaxis()->GetTitle();
    if (m_axis.histo.x.title.find("Noise Occupancy")!=std::string::npos) {
        m_axis.histo.x.low   = i_h->GetBinContent(i_h->GetMinimumBin());
        m_axis.histo.x.high  = i_h->GetBinContent(i_h->GetMaximumBin());
        m_axis.histo.x.nbins = 100;
    } else if (m_axis.histo.x.title.find("Sigma ToT")!=std::string::npos) {
        m_axis.histo.x.low   = 0;
        m_axis.histo.x.high  = 10;
        m_axis.histo.x.nbins = 100;
        m_axis.histo.y.title = m_axis.histo.y.title + " per 0.1bc";
    } else {
        double bin_width = 1;
        if (m_axis.histo.x.title.find("Threshold")!=std::string::npos) {
            bin_width = 10;
            m_axis.histo.x.low  = bin_width*std::floor((i_h->GetMinimum(1))/bin_width)-bin_width/2.;
            m_axis.histo.x.high = bin_width*std::floor((i_h->GetMaximum())/bin_width)+bin_width/2.;
            m_axis.histo.y.title = m_axis.histo.y.title + " per 10e";
        } else if (m_axis.histo.x.title.find("Noise")!=std::string::npos) {
            bin_width = 5;
            m_axis.histo.x.low  = bin_width*std::floor((i_h->GetMinimum(1))/bin_width)-bin_width/2.;
            m_axis.histo.x.high = bin_width*std::floor((i_h->GetMaximum())/bin_width)+bin_width/2.;
            m_axis.histo.y.title = m_axis.histo.y.title + " per 5e";
        } else {
            m_axis.histo.x.low  = bin_width*std::floor((i_h->GetMinimum())/bin_width)-bin_width/2.;
            m_axis.histo.x.high = bin_width*std::floor((i_h->GetMaximum())/bin_width)+bin_width/2.;
        }
        if ((m_axis.histo.x.high-m_axis.histo.x.low)==1) {
            if (m_axis.histo.x.high==0.5) m_axis.histo.x.high+=1;
            else m_axis.histo.x.low+=-1;
        }
        m_axis.histo.x.nbins = (int)((m_axis.histo.x.high-m_axis.histo.x.low)/bin_width);
    }
}
json PlotTool::getParameters() {
#ifdef DEBUG
    std::cout << "PlotTool::getParameters()" << std::endl;
#endif
    json data;

    data["Type"] = m_histo_type;

    data["x"]["Bins"] = m_h->GetNbinsX();
    data["x"]["Low"]  = m_h->GetXaxis()->GetXmin();
    data["x"]["High"] = m_h->GetXaxis()->GetXmax();

    if (m_histo_type=="Histo1d") {
        data["y"]["Bins"] = 1;
        data["y"]["Low"]  = m_h->GetBinContent(m_h->GetMinimumBin());
        data["y"]["High"] = m_h->GetBinContent(m_h->GetMaximumBin());
    } else {
        data["y"]["Bins"] = m_h->GetNbinsY();
        data["y"]["Low"]  = m_h->GetYaxis()->GetXmin();
        data["y"]["High"] = m_h->GetYaxis()->GetXmax();

        data["z"]["Bins"] = 1;
        data["z"]["Low"]  = m_h->GetBinContent(m_h->GetMinimumBin());
        data["z"]["High"] = m_h->GetBinContent(m_h->GetMaximumBin());
    }

    data["x"]["AxisTitle"] = m_h->GetXaxis()->GetTitle();
    data["y"]["AxisTitle"] = m_h->GetYaxis()->GetTitle();
    data["z"]["AxisTitle"] = m_h->GetZaxis()->GetTitle();

    if (m_h->GetXaxis()->GetLabels()) {
        for (int i=0; i<m_h->GetNbinsX(); i++) {
            data["x"]["AxisLabel"].push_back(m_h->GetXaxis()->GetBinLabel(i+1));
        }
    }
    if (m_h->GetYaxis()->GetLabels()) {
        for (int i=0; i<m_h->GetNbinsY(); i++) {
            data["y"]["AxisLabel"].push_back(m_h->GetYaxis()->GetBinLabel(i+1));
        }
    }

    data["Overwrite"] = m_axis.overwrite;

    return data;
};
//==============================
void PlotTool::setHisto(TH1* i_h, std::string i_n) {
#ifdef DEBUG
    std::cout << "PlotTool::setHisto(TH1 i_h, " << i_n << ")" << std::endl;
#endif
    m_h = (TH1*)i_h->Clone(i_n.c_str());
    this->setParameters((TH1*)m_h);
};
void PlotTool::setHisto(TH2* i_h, std::string i_n) {
#ifdef DEBUG
    std::cout << "PlotTool::setHisto(TH1 i_h, " << i_n << ")" << std::endl;
#endif
    m_h = (TH2*)i_h->Clone(i_n.c_str());
    this->setParameters((TH2*)m_h);
};
void PlotTool::setHisto(TF1* i_f) {
#ifdef DEBUG
    std::cout << "PlotTool::setHisto(TF1 i_f)" << std::endl;
#endif
    m_f = i_f;
};
//==============================
void PlotTool::setChip(std::string i_type, std::string i_name, std::string i_fe_name) {
#ifdef DEBUG
    std::cout << "PlotTool::setChip(" << i_type << ", " << i_name << ", " << i_fe_name << ")" << std::endl;
#endif
    m_chip_type = i_type;
    m_chip_name = i_name;
    m_fe_name = i_fe_name;
};
void PlotTool::setTitle(std::string i_t) {
#ifdef DEBUG
    std::cout << "PlotTool::setTitle(" << i_t << ")" << std::endl;
#endif
    m_title = i_t;
}
void PlotTool::setData(json& i_j, std::string i_name, std::string i_type) {
#ifdef DEBUG
    std::cout << "PlotTool::setData(json, " << i_name << ", " << i_type << ")" << std::endl;
#endif
    m_j           = i_j;
    m_data_name   = i_name;
    m_type        = i_type;
    m_data_format = 0;
    this->setParameters(m_j);
};
void PlotTool::setData(TH1* i_h, std::string i_name, std::string i_type) {
#ifdef DEBUG
    std::cout << "PlotTool::setData(TH1, " << i_name << ", " << i_type << ")" << std::endl;
#endif
    m_h1          = (TH1*)i_h->Clone(Form("%s_clone",i_h->GetName()));
    m_data_name   = i_name;
    m_type        = i_type;
    m_data_format = 1;
    this->setParameters(m_h1);
};
void PlotTool::setData(TH2* i_h, std::string i_name, std::string i_type) {
#ifdef DEBUG
    std::cout << "PlotTool::setData(TH2, " << i_name << ", " << i_type << ")" << std::endl;
#endif
    m_h2          = (TH2*)i_h->Clone(Form("%s_clone",i_h->GetName()));
    m_data_name   = i_name;
    m_type        = i_type;
    m_data_format = 2;
    this->setParameters(m_h2);
};
//==============================
void PlotTool::setDist(TH1* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::setDist(i_h)" << std::endl;
#endif
    m_dist = i_h;
};
void PlotTool::setMap(TH2* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::setMap(i_h)" << std::endl;
#endif
    m_map = i_h;
};
//==============================
void PlotTool::fitHisto(TH1* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::fitHisto(TH1)" << std::endl;
#endif
    TCanvas *c = new TCanvas(Form("c_fit_%s",i_h->GetName()), "", 800, 600);
    style_TH1Canvas(c);
    i_h->SetBinContent(i_h->FindBin(0),0);
    i_h->Draw();
    double mean = i_h->GetMean();
    double rms = i_h->GetRMS();

    /// Fit plot with Gaussian; for single FE plots
    std::string fit_name = "Fit_Gaus";
    if (m_fe_name!="") fit_name = fit_name + "_" + m_fe_name;
    m_f = new TF1(fit_name.c_str(), "gaus", (mean-5*rms < 0) ? -0.5 : (mean-5*rms), mean+5*rms);
    m_f->SetParameter(0, i_h->GetBinContent(i_h->FindBin(mean)));
    m_f->SetParameter(1, mean);
    m_f->SetParameter(2, rms);
    i_h->Fit(fit_name.c_str(), "0Q", "", (mean-5*rms < 0) ? -0.5 : (mean-5*rms), mean+5*rms);
    /// Change range and refit 5 times to get better fit.
    double fit_par[3];
    //double fit_err[3];
    for (int j=0; j<3; j++) {
        for (int k = 0; k<3; k++) {
            fit_par[k] = m_f->GetParameter(k);
            //fit_err[k] = m_f->GetParError(k);
        }
        double fit_min = fit_par[1] - (3*fit_par[2]);
        double fit_max = fit_par[1] + (3*fit_par[2]);
        i_h->Fit(fit_name.c_str(), "Q", "", fit_min, fit_max);
    }
    m_f->Write();
    delete c;
}
//==============================
TH1* PlotTool::getTH() {
#ifdef DEBUG
    std::cout << "PlotTool::getTH()" << std::endl;
#endif
    return (TH1*)m_h->Clone();
}
TF1* PlotTool::getTF() {
#ifdef DEBUG
    std::cout << "PlotTool::getTF()" << std::endl;
#endif
    return (TF1*)m_f->Clone();
}
//==============================
void PlotTool::setThreshold(double thr) {
#ifdef DEBUG
    std::cout << "PlotTool::setThreshold(" << thr << ")" << std::endl;
#endif
    m_thr = thr;
}
json PlotTool::setRms(TH1* i_h) {
#ifdef DEBUG
    std::cout << "PlotTool::setRms(TH1)" << std::endl;
#endif
    m_h->SetBinContent(m_h->FindBin(0),0);

    double mean = i_h->GetMean();
    double rms = i_h->GetRMS();
    std::string mean_str = Form("Mean_{hist} = %.1f", mean);
    this->setTLatexLeft(mean_str);
    std::string rms_str = Form("RMS_{hist} = %.1f", rms);
    this->setTLatexLeft(rms_str);

    json result;
    result["mean"] = mean;
    result["sigma"] = rms;
    return result;
};
json PlotTool::setRms() {
#ifdef DEBUG
    std::cout << "PlotTool::setRms()" << std::endl;
#endif
    return this->setRms(m_h);
};
json PlotTool::setGaus(TF1* i_f) {
#ifdef DEBUG
    std::cout << "PlotTool::setGaus(TF1)" << std::endl;
#endif
    m_h->SetBinContent(m_h->FindBin(0),0);

    double fit_par[3], fit_err[3];
    for (int j=0; j<3; j++) {
        for (int k = 0; k<3; k++) {
            fit_par[k] = i_f->GetParameter(k);
            fit_err[k] = i_f->GetParError(k);
        }
    }

    json result;
    if (fit_par[1] > 0) {
        std::vector <float> results;
        results = this->thresholdPercent(fit_par[1]);

        std::string mean_gaus_str  = Form("Mean_{gaus} = %.1f #pm %.1f", fit_par[1], fit_err[1]);
        this->setTLatexLeft(mean_gaus_str);
        std::string sigma_gaus_str = Form("#sigma_{gaus} = %.1f #pm %.1f", fit_par[2], fit_err[2]);
        this->setTLatexLeft(sigma_gaus_str);
        std::string chi_square_str = Form("#chi^{2} / DOF = %.1f / %i", i_f->GetChisquare(), i_f->GetNDF());
        this->setTLatexLeft(chi_square_str);
        std::string minmax_str  = Form("( %.2f / %.2f )_{%.1f%%}", results[1], results[3], m_percent);
        this->setTLatexRight(minmax_str);

        result["per"] = m_percent;
        result["min"] = results[1];
        result["max"] = results[3];
        result["zero"]["cnt"] = results[5];
        result["over"]["cnt"] = results[6];
    }

    result["mean"] = fit_par[1];
    result["sigma"] = fit_par[2];
    result["chi2"] = i_f->GetChisquare();

    m_f = (TF1*)i_f->Clone(Form("%s_clone",i_f->GetName()));
    m_gaus = true;

    return result;
};
json PlotTool::setOcc() {
#ifdef DEBUG
    std::cout << "PlotTool::setOcc()" << std::endl;
#endif
    json result;
    return result;
};
json PlotTool::setNoi() {
#ifdef DEBUG
    std::cout << "PlotTool::setNoi()" << std::endl;
#endif
    json result;
    return result;
};
json PlotTool::setZero(int zero) {
#ifdef DEBUG
    std::cout << "PlotTool::setZero(" << zero << ")" << std::endl;
#endif
    std::string zeros_str = Form("Untuned Pixels = %i", zero);
    this->setTLatexLeft(zeros_str);

    json result;
    result["cnt"] = zero;
    return result;
};
json PlotTool::setZero() {
#ifdef DEBUG
    std::cout << "PlotTool::setZero()" << std::endl;
#endif
    return this->setZero(m_zeros);
};
//==============================
/////////////////
/// Protected ///
/////////////////
//==============================
void PlotTool::writeHisto() {
    m_h->Write();
}
void PlotTool::buildHisto() {
#ifdef DEBUG
    std::cout << "PlotTool::buildHisto()" << std::endl;
#endif
    if (m_type=="")    m_histo_name = m_data_name;
    else               m_histo_name = m_type;
    if (m_fe_name!="") m_histo_name = m_histo_name + "_" + m_fe_name;
    std::replace(m_histo_name.begin(), m_histo_name.end(), '-', '_');
}
void PlotTool::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
    m_c->cd();

    TLatex *tname= new TLatex();
    style_TLatex(tname, 22, 73, 20);
    std::string latex;

    /// Write Chip Type on the Canvas
    latex = m_chip_type;
    tname->DrawLatex(0.2, 0.96, latex.c_str());

    /// Write Chip Name on the Canvas
    latex = "Chip SN: " + m_chip_name;
    tname->DrawLatex(0.8, 0.96, latex.c_str());

    latex = m_title;
    tname->DrawLatex(0.5, 0.92, latex.c_str());

    this->setText();
    m_c->Update();

    std::string title;
    /// print PDF
    title = i_dir + "/" + m_chip_name + ".pdf";
    m_c->Print(title.c_str());

    /// print other format (default: png)
    if (i_print) {
        title = i_dir + "/" + m_chip_name + "_" + m_title + "." + i_ext;
        m_c->Print(title.c_str());
    }
    delete m_c;
};
void PlotTool::setText() {
    /// Write the Gaussian mean/sigma and the histogram mean/rms on the Canvas.
    TLatex *tlatex = new TLatex;
    style_TLatex(tlatex, 13, 63, 20);
    double pos_x, pos_y;
    std::vector <std::string> latexs;

    /// left
    pos_x = 0.18;
    pos_y = 0.88;
    latexs = this->getTLatexLeft();
    for (auto text : latexs) {
        double p_x = pos_x;
        double p_y = pos_y;
        tlatex->DrawLatex(p_x, p_y, text.c_str());
        pos_y = pos_y-0.05;
    }

    /// right
    pos_x = 0.63;
    pos_y = 0.88;
    if (m_fe_name!=""&&m_histo_type=="Histo1d") {
        TLegend* l = new TLegend(pos_x, pos_y-0.06, 0.85, pos_y);
        l->AddEntry(m_h, m_fe_name.c_str(), "f");
        l->SetBorderSize(0);
        l->Draw();
        pos_y = pos_y-0.06;
    }
    latexs = this->getTLatexRight();
    for (auto text : latexs) {
        double p_x = pos_x;
        double p_y = pos_y;
        tlatex->DrawLatex(p_x, p_y, text.c_str());
        pos_y = pos_y-0.05;
    }
};
void PlotTool::setTLatexLeft(std::string i_text) {
    m_latex_left.push_back(i_text);
};
void PlotTool::setTLatexRight(std::string i_text) {
    m_latex_right.push_back(i_text);
};
std::vector <std::string> PlotTool::getTLatexLeft() {
    return m_latex_left;
};
std::vector <std::string> PlotTool::getTLatexRight() {
    return m_latex_right;
};
//==============================
int PlotTool::whichSigma(double i_value, double i_mean, double i_sigma, double i_bin_width, int i_bin_num) {
    int hist_bin = -1;
    for (int i=1; i<i_bin_num+1; i++) {
        if ( fabs(i_mean-i_value) >= ((i-1)*i_bin_width*i_sigma) && fabs(i_mean-i_value) < (i*i_bin_width*i_sigma) ) hist_bin=i;
    }
    if ( fabs(i_mean-i_value) >= (i_bin_num*i_bin_width*i_sigma) ) hist_bin=i_bin_num;
    else if ( fabs(i_mean-i_value) < (i_bin_width*i_sigma) ) hist_bin=1;

    return hist_bin;
}
int PlotTool::whichFE(int i_row, int i_col) {
    int which_fe = 0; //add if 10, give error in .cxx file
    if (m_chip_type=="RD53A") {
        int division1 = 128, division2 = 264;
        if      (i_col<division1)                     which_fe = 1; /// Syn FE
        else if (i_col>=division1 && i_col<division2) which_fe = 2; /// Lin FE
        else if (i_col>=division2)                    which_fe = 3; /// Diff FE
    } else if (m_chip_type=="FE-I4B") {
        which_fe = 0;
    }
    return which_fe;
}
//==============================
void PlotTool::style_TH1(TH1* i_h, std::string i_Xtitle, std::string i_Ytitle, int i_color){
    i_h->SetStats(0);
    i_h->SetTitle(0);
    if (i_Xtitle!="") i_h->GetXaxis()->SetTitle(i_Xtitle.c_str());
    if (i_Ytitle!="") i_h->GetYaxis()->SetTitle(i_Ytitle.c_str());
    i_h->GetXaxis()->SetTitleSize(0.045);
    i_h->GetXaxis()->SetTitleOffset(1.25);
    i_h->GetYaxis()->SetTitleSize(0.045);
    i_h->GetYaxis()->SetTitleOffset(1.7);
    i_h->GetXaxis()->SetLabelSize(0.04);
    i_h->GetYaxis()->SetLabelSize(0.04);
    i_h->SetFillColor(i_color);
    i_h->SetLineWidth(0);
}
void PlotTool::style_TGraph(TGraph* i_h, std::string i_Xtitle, std::string i_Ytitle, int i_color){
    i_h->SetTitle(0);
    if (i_Xtitle!="") i_h->GetXaxis()->SetTitle(i_Xtitle.c_str());
    if (i_Ytitle!="") i_h->GetYaxis()->SetTitle(i_Ytitle.c_str());
    i_h->GetXaxis()->SetTitleSize(0.045);
    i_h->GetXaxis()->SetTitleOffset(1.25);
    i_h->GetYaxis()->SetTitleSize(0.045);
    i_h->GetYaxis()->SetTitleOffset(1.7);
    i_h->GetXaxis()->SetLabelSize(0.04);
    i_h->GetYaxis()->SetLabelSize(0.04);
    i_h->SetMarkerColor(i_color);
    i_h->SetLineColor(i_color);
}
void PlotTool::style_THStack(THStack* i_h, std::string i_Xtitle, std::string i_Ytitle){
    i_h->SetTitle(0);
    if (i_Xtitle!="") i_h->GetXaxis()->SetTitle(i_Xtitle.c_str());
    if (i_Ytitle!="") i_h->GetYaxis()->SetTitle(i_Ytitle.c_str());
    i_h->GetXaxis()->SetTitleSize(0.045);
    i_h->GetXaxis()->SetTitleOffset(1.25);
    i_h->GetYaxis()->SetTitleSize(0.045);
    i_h->GetYaxis()->SetTitleOffset(1.7);
    i_h->GetXaxis()->SetLabelSize(0.04);
    i_h->GetYaxis()->SetLabelSize(0.04);
}
void PlotTool::style_TH2(TH2* i_h, std::string i_Xtitle, std::string i_Ytitle, std::string i_Ztitle, int i_color){
    i_h->SetStats(0);
    i_h->SetTitle(0);
    if (i_Xtitle!="") i_h->GetXaxis()->SetTitle(i_Xtitle.c_str());
    if (i_Ytitle!="") i_h->GetYaxis()->SetTitle(i_Ytitle.c_str());
    if (i_Ztitle!="") i_h->GetZaxis()->SetTitle(i_Ztitle.c_str());
    i_h->GetXaxis()->SetTitleSize(0.045);
    i_h->GetXaxis()->SetTitleOffset(1.25);
    i_h->GetYaxis()->SetTitleSize(0.045);
    i_h->GetYaxis()->SetTitleOffset(1.7);
    i_h->GetZaxis()->SetTitleSize(0.045);
    i_h->GetZaxis()->SetTitleOffset(1.6);
    i_h->GetXaxis()->SetLabelSize(0.04);
    i_h->GetYaxis()->SetLabelSize(0.04);
    i_h->GetZaxis()->SetLabelSize(0.04);
    gStyle->SetNumberContours(255);
    gStyle->SetPalette(i_color);
}
void PlotTool::style_TF1(TF1* i_f, int i_color){
    i_f->SetTitle(0);
    i_f->SetLineColor(i_color);
}
//==============================
void PlotTool::style_TH1Canvas(TCanvas *i_c){
    i_c->SetTopMargin(0.1);
    i_c->SetLeftMargin(0.15);
    i_c->SetRightMargin(0.08);
    i_c->SetBottomMargin(0.1225);
}
void PlotTool::style_TH2Canvas(TCanvas *i_c){
    i_c->SetTopMargin(0.1);
    i_c->SetLeftMargin(0.15);
    i_c->SetRightMargin(0.20);
    i_c->SetBottomMargin(0.1225);
}
void PlotTool::style_THStackCanvas(TCanvas *i_c){
    i_c->SetTopMargin(0.1);
    i_c->SetLeftMargin(0.15);
    i_c->SetRightMargin(0.10);
    i_c->SetBottomMargin(0.1225);
}
//==============================
void PlotTool::style_TLatex(TLatex* i_latex, int i_align, int i_font, int i_size, int i_color){
    i_latex->SetNDC();
    i_latex->SetTextAlign(i_align);
    i_latex->SetTextFont(i_font);
    i_latex->SetTextSizePixels(i_size);
    i_latex->SetTextColor(i_color);
}
//==============================
int PlotTool::getFE(std::string n) {
    int fe = 0;
    if      (n=="Syn")  fe = 1;
    else if (n=="Lin")  fe = 2;
    else if (n=="Diff") fe = 3;
    return fe;
}
int PlotTool::getFE() {
    return this->getFE(m_fe_name);
}
//==============================
std::vector <float> PlotTool::thresholdPercent(double gaussMean) {
    int nbinsx = m_map->GetNbinsX();
    int nbinsy = m_map->GetNbinsY();
    int zeroCounter = 0;
    int overCounter = 0;
    std::vector < std::pair<float,float> > mean_Diff;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double pix_value = m_map->GetBinContent(col+1, row+1);
            if (pix_value<=0) zeroCounter++;
            else if (pix_value > 5*gaussMean) overCounter++;
            else mean_Diff.push_back( std::make_pair(pix_value, (fabs(pix_value-gaussMean))) );
        }
    }
    float numPixels = nbinsx*nbinsy;
    int numPix = (numPixels - (zeroCounter+overCounter)) * m_percent * 0.01;

    std::sort (mean_Diff.begin(), mean_Diff.end(), sortbysec); //sort from least to greatest for the second element of the pair.

    float vectorSize = mean_Diff.size();

    //Find the value corresponding to X% num of pix. Look at the values around X% of pixels and choose the min/max from there. Save how many iterations it took.
    float diffMax=99999999.0, diffMin=999999999.0, tryMax=0., tryMin=0.;
    for (int i=0; i<20000; i++) {
        if (mean_Diff[numPix-i].first > gaussMean) {
            diffMax = mean_Diff[numPix-i].first;
            tryMax=i*-1;
            break;
        }
        else if (mean_Diff[numPix+i].first > gaussMean && numPix+i < vectorSize) {
            diffMax = mean_Diff[numPix+i].first;
            tryMax=i;
            break;
        }
    }

    for (int i=0; i<20000; i++) {
        if (mean_Diff[numPix-i].first < gaussMean) {
            diffMin = mean_Diff[numPix-i].first;
            tryMin=i*(-1);
            break;
        }
        else if (mean_Diff[numPix+i].first < gaussMean && numPix+i < vectorSize) {
            diffMin = mean_Diff[numPix+i].first;
            tryMin=i;
            break;
        }
    }

    float checkSize = numPixels - (zeroCounter+overCounter);
    if ( vectorSize != checkSize ) {
        diffMax = -100000;
        diffMin = -100000;
    }
    float maxMin[7] = {vectorSize, diffMin, tryMin, diffMax, tryMax, (float)zeroCounter, (float)overCounter};
    std::vector <float> results;
    for (int i=0; i<7; i++) {
            results.push_back(maxMin[i]);
    }

    return results;
}
//==============================
bool sortbysec(const std::pair<double, float> &a, const std::pair<double, float> &b) {	//sort using second element of pairs in vector.
        return (a.second < b.second);
}
