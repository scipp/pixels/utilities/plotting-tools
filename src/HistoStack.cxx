#include "HistoStack.h"

//////////////
/// Public ///
//////////////
//==============================
HistoStack::HistoStack() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoStack::HistoStack()" << std::endl;
#endif
};
HistoStack::~HistoStack(){
#ifdef DEBUG
    std::cout << "HistoStack::~HistoStack()" << std::endl;
#endif
};
//==============================
/////////////////
/// Protected ///
/////////////////
//==============================
void HistoStack::writeHisto() {
    m_hs->Write();
}
void HistoStack::buildHisto() {
#ifdef DEBUG
    std::cout << "HistoStack::buildHisto()" << std::endl;
#endif
    PlotTool::buildHisto();
    m_histo_name = m_histo_name + "_Stack";

    m_hs = new THStack(m_histo_name.c_str(), "");
    for (auto h : m_vec_h) {
        style_TH1((TH1*)h, h->GetXaxis()->GetTitle(), h->GetYaxis()->GetTitle(), m_color.at(PlotTool::getFE(h->GetName())));
        m_hs->Add(h);
    }
}
void HistoStack::setCanvas() {
#ifdef DEBUG
    std::cout << "HistoStack::setCanvas()" << std::endl;
#endif
    m_c = new TCanvas(Form("canv_%s",m_hs->GetName()), "", 800, 600);
    style_THStackCanvas(m_c);
};
void HistoStack::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoStack::drawHisto()" << std::endl;
#endif
    m_c->cd();
    m_hs->Draw();
    style_THStack(m_hs, m_axis.histo.x.title, m_axis.histo.y.title);
    if (m_axis.histo.x.nbins==2) {
        m_hs->GetXaxis()->SetNdivisions(2,0,0);
    }
    if (m_axis.overwrite) {
        m_hs->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
        m_hs->GetYaxis()->SetRangeUser(m_axis.histo.y.low, m_axis.histo.y.high);
    }
    m_c->Modified();
    m_c->Update();
};
void HistoStack::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
#ifdef DEBUG
    std::cout << "HistoStack::printHisto(" << i_dir << ", " << i_print << ", " << i_ext << ")" << std::endl;
#endif
    m_c->cd();

    // right
    double pos_y = 0.88;
    TLegend* l = new TLegend(0.33, pos_y-0.06, 0.9, pos_y);
    l->SetNColumns(m_vec_h.size());
    for (auto h : m_vec_h) {
        l->AddEntry(h, h->GetName(), "f");
    }
    l->SetBorderSize(0);
    l->SetFillStyle(0);
    l->SetFillColor(0);
    l->Draw();

    m_hs->SetMaximum(m_hs->GetMaximum()*1.25);
    m_c->Modified();
    m_c->Update();
    PlotTool::printHisto(i_dir, i_print, i_ext);
};
void HistoStack::setHisto(TH1* i_h, std::string i_n) {
#ifdef DEBUG
    std::cout << "HistoStack::setHisto(TH1 i_h, " << i_n << ")" << std::endl;
#endif
    TH1* h_clone = (TH1*)i_h->Clone(i_n.c_str());
    this->setParameters((TH1*)h_clone);
    m_vec_h.push_back(h_clone);
}
void HistoStack::setHisto(TH2* i_h, std::string i_n) {
#ifdef DEBUG
    std::cout << "HistoStack::setHisto(TH2 i_h, " << i_n << ")" << std::endl;
#endif
    TH1* h_clone = (TH2*)i_h->Clone(i_n.c_str());
    this->setParameters((TH2*)h_clone);
    m_vec_h.push_back(h_clone);
}
//==============================
