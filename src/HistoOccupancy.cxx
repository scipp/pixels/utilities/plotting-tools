#include "HistoOccupancy.h"

//////////////
/// Public ///
//////////////
//==============================
HistoOccupancy::HistoOccupancy() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoOccupancy::HistoOccupancy()" << std::endl;
#endif
};
HistoOccupancy::~HistoOccupancy(){
#ifdef DEBUG
    std::cout << "HistoOccupancy::~HistoOccupancy()" << std::endl;
#endif
};
//==============================
///////////////
/// Private ///
///////////////
//==============================
void HistoOccupancy::setParametersX(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoOccupancy::setParametersX(TH2)" << std::endl;
#endif
    m_axis.histo.x.nbins = 6;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 6;
    m_axis.histo.x.title = "Occupancy [%]";
    std::string label[6] = { "0%", " 0-98%", " 98-100%", "100%", "100-102%", " >102%"};
    for (int i=0; i<6; i++) {
        m_axis.histo.x.label.push_back(label[i]);
    }
    m_thr = 100;
}
void HistoOccupancy::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoOccupancy::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    m_c->cd();
    m_h->Draw("TEXT0 SAME");
    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.25));
    m_c->Modified();
    m_c->Update();
};
void HistoOccupancy::fillHisto(json& i_j) {
#ifdef DEBUG
    std::cout << "HistoOccupancy::fillHisto(json)" << std::endl;
#endif
    int nbinsx = i_j["x"]["Bins"];
    int nbinsy = i_j["y"]["Bins"];
    m_zeros = 0;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_j["Data"][col][row];
            int bin_num = whichBin(m_thr, tmp);
            m_h->AddBinContent(bin_num);
        }
    }
}
void HistoOccupancy::fillHisto(TH2* i_h) {
#ifdef DEBUG
    std::cout << "HistoOccupancy::fillHisto(TH2)" << std::endl;
#endif
    int nbinsx = i_h->GetNbinsX();
    int nbinsy = i_h->GetNbinsY();
    m_zeros = 0;
    int fe = PlotTool::getFE();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            if (fe!=0&&PlotTool::whichFE(row, col)!=fe) continue;
            double tmp = i_h->GetBinContent(col+1, row+1);
            int bin_num = whichBin(m_thr, tmp);
            m_h->AddBinContent(bin_num);
        }
    }
};
int HistoOccupancy::whichBin(double value, double occnum) {
#ifdef DEBUG
    std::cout << "HistoNoiseOccupancy::whichBin(" << value << ", " << occnum << ")" << std::endl;
#endif
    int hist_bin = -1;
    double zero = 0.0;
    double ninety8 = value * 0.98;
    double hundred02 = value * 1.02;
    if      (occnum == zero)                        hist_bin = 1;
    else if (occnum > zero && occnum < ninety8)     hist_bin = 2;
    else if (occnum >= ninety8 && occnum < value)   hist_bin = 3;
    else if (occnum == value)                       hist_bin = 4;
    else if (occnum > value && occnum <= hundred02) hist_bin = 5;
    else if (occnum > hundred02)                    hist_bin = 6;

    return hist_bin;
}
