#include "HistoTDACStack.h"

//////////////
/// Public ///
//////////////
//==============================
HistoTDACStack::HistoTDACStack() :
HistoStack()
{
#ifdef DEBUG
    std::cout << "HistoTDACStack::HistoTDACStack()" << std::endl;
#endif
};
HistoTDACStack::~HistoTDACStack(){
#ifdef DEBUG
    std::cout << "HistoTDACStack::~HistoTDACStack()" << std::endl;
#endif
};
//==============================
/////////////////
/// Protected ///
/////////////////
//==============================
void HistoTDACStack::buildHisto() {
#ifdef DEBUG
    std::cout << "HistoTDACStack::buildHisto()" << std::endl;
#endif
    PlotTool::buildHisto();
    m_histo_name = m_histo_name + "_Stack";

    m_hs = new THStack(m_histo_name.c_str(), "");
    for (auto h : m_vec_h) {
        style_TH1((TH1*)h, h->GetXaxis()->GetTitle(), h->GetYaxis()->GetTitle(), m_color.at(PlotTool::getFE(h->GetName())));
        h->SetLineColor(1);
        h->SetLineWidth(1);
        m_hs->Add(h);
    }
}
void HistoTDACStack::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoTDACStack::drawHisto()" << std::endl;
#endif
    HistoStack::drawHisto();
    m_c->cd();
    gStyle->SetPalette(kBird);
    m_hs->Draw("histo PFC");
    m_c->Modified();
    m_c->Update();
};
void HistoTDACStack::printHisto(std::string i_dir, bool i_print, std::string i_ext) {
#ifdef DEBUG
    std::cout << "HistoTDACStack::printHisto(" << i_dir << ", " << i_print << ", " << i_ext << ")" << std::endl;
#endif
    m_c->cd();

    // right
    TLegend* l = new TLegend(0.91, 0.12, 0.99, 0.93);
    l->SetHeader("TDAC","");
    for (auto h : m_vec_h) {
        l->AddEntry(h, h->GetName(), "f");
    }
    l->SetBorderSize(0);
    l->SetFillStyle(0);
    l->SetFillColor(0);
    l->SetTextSize(0.03);
    l->Draw();

    m_hs->SetMaximum(m_hs->GetMaximum()*1.25);
    m_c->Modified();
    m_c->Update();
    PlotTool::printHisto(i_dir, i_print, i_ext);
};
//==============================
